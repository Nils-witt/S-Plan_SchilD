/*
 * Copyright (c) 2020.
 */

package de.nils_witt.splan;

public class Config {
    private String bearer;
    private String url;

    public String getBearer() {
        return bearer;
    }

    public String getUrl() {
        return url;
    }
}
