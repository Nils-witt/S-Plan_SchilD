# S-Plan SchilDConnector

This program sets the Courses for Students based on a XSLX file from SchiLD NRW.

[![Build Status](https://travis-ci.com/Nils-witt/S-Plan_SchilD.svg?branch=master)](https://travis-ci.com/Nils-witt/S-Plan_SchilD)
